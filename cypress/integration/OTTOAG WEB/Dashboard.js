describe('OTTOAG BDD', function() {

beforeEach(function () {
  cy.log('I run before every test in every spec file!!!!!!')
})

	
  it('Go To OttoAG Webpage', function() {
    cy.visit('https://otto-dev.ottopay.id/ottoag/#/')

    cy.contains('Login').click()

    cy.url().should('include', '/#')

})

  it('OttoAG Login', function() {


    cy.get('#mat-input-0')
      .type('tio')
      .should('have.value', 'tio')

    cy.get('#mat-input-1')
      .type('testing123')
      .should('have.value', 'testing123')

    cy.contains('Login').click()

    cy.url().should('include', '/#/main')

       	cy.wait(500)
       	cy.reload()



  })

   it('OttoAG Dashboard', function(){

   	cy.get(':nth-child(2) > .mat-button-wrapper').click()

   	cy.get(':nth-child(2) > .mat-button-wrapper').click()	

   	cy.wait(500)
 

   	cy.contains('Biller')

   	cy.contains('Settings')

   	cy.contains('Authorization')	

   	cy.contains('Biller').click()
   	cy.wait(500)

   	cy.contains('Biller Subscriber')

   	cy.contains('Key Management')

   	cy.contains('Biller Supplier')

   	cy.contains('Settings').click()

   })

   it('OttoAG Biller Menu', function() {

   })


})