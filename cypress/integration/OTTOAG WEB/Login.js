describe('OTTOAG BDD', function() {

beforeEach(function () {
  cy.log('I run before every test in every spec file!!!!!!')
})

	
  it('Go To OttoAG Webpage', function() {
    cy.visit('https://otto-dev.ottopay.id/ottoag/#/')

    cy.contains('Login').click()

    cy.url().should('include', '/#')

})

  it('OttoAG Login', function() {


    cy.get('#mat-input-0')
      .type('tio')
      .should('have.value', 'tio')

    cy.get('#mat-input-1')
      .type('testing123')
      .should('have.value', 'testing123')

    cy.contains('Login').click()

    cy.url().should('include', '/#/main')

       	cy.wait(500)
       	cy.reload()

   })


})